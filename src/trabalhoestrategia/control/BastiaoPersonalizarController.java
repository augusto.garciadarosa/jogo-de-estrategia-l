/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhoestrategia.control;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import trabalhoestrategia.model.Bastiao;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class BastiaoPersonalizarController implements Initializable {
  /**
     * Initializes the controller class.
     */
    
    int numPontos=10;
    
    @FXML
    private Label vida;
    @FXML
    private Label dano;
    @FXML
    private Label pontos;
    @FXML
    private void maisVida(){
        if (numPontos>0){
        Bastiao.maisVida();
        numPontos--;
        danoEVida();
    }
    }
    private void danoEVida(){
        vida.setText(Integer.toString(15+Bastiao.getPerVida()));
        dano.setText(Integer.toString(6+Bastiao.getPerAtaque()));
        pontos.setText(Integer.toString(numPontos));
    }
       
    @FXML
    private void menosVida(){
        if (Bastiao.getPerVida()>0||Bastiao.getPerVida()>-14){
            Bastiao.menosVida();
            numPontos++;
        }
        danoEVida();
    
    }
    @FXML
    private void maisAtaque(){
        if (numPontos>0){
        Bastiao.maisAtaque();
        numPontos--;
        danoEVida();
    }
    
    }
    @FXML
    private void menosAtaque(){
         if (Bastiao.getPerAtaque()>0||Bastiao.getPerAtaque()>-5){
            Bastiao.menosAtaque();
            numPontos++;
            danoEVida();
        }
    
    }
    @FXML
    private void trocarExercito(){
        TrabalhoEstrategia.trocaTela("TelaCriacaoExercito.fxml");  
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        vida.setText(Integer.toString(Bastiao.getPerVida()+Bastiao.getVidaNormal()));
        dano.setText(Integer.toString(Bastiao.getPerAtaque()+Bastiao.getDanoNormal()));
        numPontos = 10-Bastiao.getPerVida()-Bastiao.getPerAtaque();
        pontos.setText(Integer.toString(numPontos));
    }    
    
}