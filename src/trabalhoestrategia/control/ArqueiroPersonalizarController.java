/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhoestrategia.control;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import trabalhoestrategia.model.Arqueiro;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class ArqueiroPersonalizarController implements Initializable {

   int numPontos=0;
    
    @FXML
    private Label vida;
    @FXML
    private Label dano;
    @FXML
    private Label pontos;
    @FXML
    private void maisVida(){
        if (numPontos>0){
        Arqueiro.maisVida();
        numPontos--;
        danoEVida();
    }
    }
    private void danoEVida(){
        vida.setText(Integer.toString(7+Arqueiro.getPerVida()));
        dano.setText(Integer.toString(8+Arqueiro.getPerAtaque()));
        pontos.setText(Integer.toString(numPontos));
    }
       
    @FXML
    private void menosVida(){
        if (Arqueiro.getPerVida()>0||Arqueiro.getPerVida()>-6){
            Arqueiro.menosVida();
            numPontos++;
        }
        danoEVida();
    
    }
    @FXML
    private void maisAtaque(){
        if (numPontos>0){
        Arqueiro.maisAtaque();
        numPontos--;
        danoEVida();
    }
    
    }
    @FXML
    private void menosAtaque(){
         if (Arqueiro.getPerAtaque()>0||Arqueiro.getPerAtaque()>-7){
            Arqueiro.menosAtaque();
            numPontos++;
            danoEVida();
        }
    
    }
    @FXML
    private void trocarExercito(){
        TrabalhoEstrategia.trocaTela("TelaCriacaoExercito.fxml");
        
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        vida.setText(Integer.toString(Arqueiro.getPerVida()+Arqueiro.getVidaNormal()));
        dano.setText(Integer.toString(Arqueiro.getPerAtaque()+Arqueiro.getDanoNormal()));
        numPontos = 10-Arqueiro.getPerVida()-Arqueiro.getPerAtaque();
        pontos.setText(Integer.toString(numPontos));
    }    
    
}
