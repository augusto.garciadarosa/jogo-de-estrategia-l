/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhoestrategia.control;

import trabalhoestrategia.model.Arqueiro;
import trabalhoestrategia.model.Bastiao;
import trabalhoestrategia.model.Guerreiro;
import trabalhoestrategia.model.Lanceiro;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class TelaVitoriaeDerrotaController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private Label aliGuerreiroL;
    @FXML
    private Label aliArqueiroL;
    @FXML
    private Label aliLanceiroL;
    @FXML
    private Label aliBastiaoL;
    @FXML
    private Label iniZombieL;
    @FXML
    private Text texto;
    @FXML
    private Label iniGuerreiroL;
    @FXML
    private Label iniArqueiroL;
    @FXML
    private Label iniLanceiroL;
    @FXML
    private Label iniBastiaoL;
    
    private static int uniGuerreiroM=0;
    private static int uniArqueiroM=0;
    private static int uniLanceiroM=0;
    private static int uniBastiaoM=0;
    
    private static int aliGuerreiroM=0;
    private static int aliArqueiroM=0;
    private static int aliLanceiroM=0;
    private static int aliBastiaoM=0;
    
    private static int inizombie=0;

    private static boolean vitoria=false;
    private static boolean temZombi=false;

    public static boolean isTemZombi() {
        return temZombi;
    }

    public static void setTemZombi(boolean temZombi) {
        TelaVitoriaeDerrotaController.temZombi = temZombi;
    }
    
    public static boolean isVitoria() {
        return vitoria;
    }

    public static void setVitoria(boolean vitoria) {
        TelaVitoriaeDerrotaController.vitoria = vitoria;
    }
    
    public static int getInizombie() {
        return inizombie;
    }

    public static void setInizombie(int inizombie) {
        TelaVitoriaeDerrotaController.inizombie = inizombie;
    }
    
    
    
    public static void setMortos(int a, int b, int c, int d, int e, int f, int g, int h){
    uniGuerreiroM=a;
    uniArqueiroM=b;
    uniLanceiroM=c;
    uniBastiaoM=d;
    aliGuerreiroM=e;
    aliArqueiroM=f;
    aliLanceiroM=g;
    aliBastiaoM=h;
    }
    public static void setMortosAliados(int e, int f, int g, int h){

    aliGuerreiroM=e;
    aliBastiaoM=f;
    aliLanceiroM=g;
    aliArqueiroM=h;
    }
    @FXML
    private void voltar(){
  
        uniGuerreiroM=0;
        uniArqueiroM=0;
        uniLanceiroM=0;
        uniBastiaoM=0;
        aliGuerreiroM=0;
        aliArqueiroM=0;
        aliLanceiroM=0;
        aliBastiaoM=0;
        inizombie=0;
        temZombi=false;
        
        TrabalhoEstrategia.trocaTela("TelaEscolhaBatalha.fxml");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       if(vitoria){
           texto.setText("Vitoria");
       }
       else{
           texto.setText("Derrota..");
       }
        iniGuerreiroL.setText("Guerreiros Mortos: " +  uniGuerreiroM);
        iniArqueiroL.setText("Arqueiros Mortos: " +  uniArqueiroM);
        iniLanceiroL.setText("Lanceiros Mortos: " +  uniLanceiroM);
        iniBastiaoL.setText("Bastioes Mortos: " +  uniBastiaoM);
    
        aliGuerreiroL.setText("Guerreiros Mortos: " +  aliGuerreiroM);
        aliArqueiroL.setText("Arqueiros Mortos: " +  aliArqueiroM);
        aliLanceiroL.setText("Lanceiros Mortos: " +  aliLanceiroM);
        aliBastiaoL.setText("Bastioes Mortos: " +  aliBastiaoM);
        if (temZombi){
            iniZombieL.setOpacity(1);
            iniZombieL.setText("Zombies Mortos: " +  inizombie);
        }
        else{
            iniZombieL.setOpacity(0);
            iniZombieL.setText("Zombies Mortos: " +  inizombie);
        }
       
    }    
    
}
