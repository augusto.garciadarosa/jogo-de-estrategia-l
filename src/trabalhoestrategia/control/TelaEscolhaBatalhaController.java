/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhoestrategia.control;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

/**
 * FXML Controller class
 *
 * @author Raynan
 */
public class TelaEscolhaBatalhaController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private void batalha1(){
        TelaCriacaoExercito.setEscolha(0);
        TelaCriacaoExercito.setBatalhaEntrada(true);
        TrabalhoEstrategia.trocaTela("TelaCriacaoExercito.fxml");
        
    }
    @FXML
    private void batalha2(){
        TelaCriacaoExercito.setEscolha(1);
        TelaCriacaoExercito.setBatalhaEntrada(true);
        TrabalhoEstrategia.trocaTela("TelaCriacaoExercito.fxml");
    }
    @FXML
    private void batalha3(){
        TelaCriacaoExercito.setEscolha(2);
        TelaCriacaoExercito.setBatalhaEntrada(true);
        TrabalhoEstrategia.trocaTela("TelaCriacaoExercito.fxml");
    }
    @FXML
    private void batalha4(){
        TelaCriacaoExercito.setEscolha(3);
        TelaCriacaoExercito.setBatalhaEntrada(true);
        TrabalhoEstrategia.trocaTela("TelaCriacaoExercito.fxml");
    }
    @FXML
    private void batalhaFinal(){
        TelaCriacaoExercito.setEscolha(4);
        TelaCriacaoExercito.setBatalhaEntrada(true);
        TrabalhoEstrategia.trocaTela("TelaCriacaoExercito.fxml");
    }
    @FXML
    private void voltar(){
        TrabalhoEstrategia.trocaTela("Menu.fxml");
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
