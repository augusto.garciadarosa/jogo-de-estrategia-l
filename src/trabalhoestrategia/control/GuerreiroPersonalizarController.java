/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhoestrategia.control;

import trabalhoestrategia.model.Guerreiro;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class GuerreiroPersonalizarController implements Initializable {

    /**
     * Initializes the controller class.
     */
    
    int numPontos=10;
    
    @FXML
    private Label vida;
    @FXML
    private Label dano;
    @FXML
    private Label pontos;
    @FXML
    private void maisVida(){
        if (numPontos>0){
        Guerreiro.maisVida();
        numPontos--;
        danoEVida();
    }
    }
    private void danoEVida(){
        vida.setText(Integer.toString(10+Guerreiro.getPerVida()));
        dano.setText(Integer.toString(3+Guerreiro.getPerAtaque()));
        pontos.setText(Integer.toString(numPontos));
    }
       
    @FXML
    private void menosVida(){
        if (Guerreiro.getPerVida()>0||Guerreiro.getPerVida()>-9){
            Guerreiro.menosVida();
            numPontos++;
        }
        danoEVida();
    
    }
    @FXML
    private void maisAtaque(){
        if (numPontos>0){
        Guerreiro.maisAtaque();
        numPontos--;
        danoEVida();
    }
    
    }
    @FXML
    private void menosAtaque(){
         if (Guerreiro.getPerAtaque()>0||Guerreiro.getPerAtaque()>-2){
            Guerreiro.menosAtaque();
            numPontos++;
            danoEVida();
        }
    
    }
    @FXML
    private void trocarExercito(){
        TrabalhoEstrategia.trocaTela("TelaCriacaoExercito.fxml");
        
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        vida.setText(Integer.toString(Guerreiro.getPerVida()+Guerreiro.getVidaNormal()));
        dano.setText(Integer.toString(Guerreiro.getPerAtaque()+Guerreiro.getDanoNormal()));
        numPontos = 10-Guerreiro.getPerVida()-Guerreiro.getPerAtaque();
        pontos.setText(Integer.toString(numPontos));
    }    
    
}
