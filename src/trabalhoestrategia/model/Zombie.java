/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhoestrategia.model;

/**
 * Controla a classe zumbi
 * - Subclasse de unidade -
 * @author Raynan
 */
public class Zombie extends Unidade{
    private static int perVida = 0;
    private static int perAtaque = 0;
    private static int qtd=0;
    private static boolean movimentoEspecial = false;
    private boolean jaMorreuUmaVez = false;

    /**
     * Guarda os principais atributos da classe zumbi
     */
    public Zombie(){
    this.setPontosDeVida(40);
    this.setArmadura(0);
    this.setPenetracaoDeArmadura(0);
    this.setDanoPorAtaque(10);
    this.setLongRange(false);
    }
  

 
    @Override
    public String toString() {
        return "Atributos: " + "Pontos De Vida: " + pontosDeVida + "; Armadura: " + armadura + "; Penetração De Armadura: " + penetracaoDeArmadura + "; Dano Por Ataque: " + danoPorAtaque + "; Consegue Atacar de Longe: " + longRange;
    }
   
    /**
     * Retorna o estado do movimento especial
     * @return
     */
    public static boolean isMovimentoEspecial() {
        return Zombie.movimentoEspecial;
    }

    /**
     * Seta o movimento especial do zumbi
     * @param a
     */
    public static void setMovimentoEspecial(boolean a) {
        Zombie.movimentoEspecial = a;
    }
    
    /**
     * Calcula o dano recebido pela unidade
     * @param a
     */
    @Override
    public boolean receberDano (Unidade a){
       if (this.pontosDeVida<=0){
          
           return true;
        }
        else{
           if(this.pontosDeVida-calculoDano(a)<=0&&jaMorreuUmaVez==false){
               jaMorreuUmaVez=true;
               this.pontosDeVida=1;
               return false;
           }
           else{
                this.pontosDeVida-=calculoDano(a);
                if (this.pontosDeVida<=0)
                    return true;
                else
                    return false;
           }
        } 
    }
    /**
     * Calcula o dano da unidade
     * @param a
     */
   
}
