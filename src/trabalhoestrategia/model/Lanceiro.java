/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhoestrategia.model;

import trabalhoestrategia.model.Unidade;

/**
 * Classe com os atributos e métodos relacionados aos guerreiros
 * - Subclasse de unidade -
 * @author Aluno
 */
public class Lanceiro extends Unidade{
    private static int perVida = 0;
    private static int perAtaque = 0;
    private static int qtd=0;
    private static boolean  movimentoEspecial = false;
   
    /**
     * Seta os atributos do lanceiro
     */
    public Lanceiro(){
    this.setPontosDeVida(6);
    this.setArmadura(2);
    this.setPenetracaoDeArmadura(5);
    this.setDanoPorAtaque(5);
    this.setLongRange(false);
    }

    /**
     * Retorna a quantidade de lanceiros
     * @return
     */
    public static int getQtd() {
        return Lanceiro.qtd;
    }

    /**
     * Seta a quantidade de lanceiros
     * @param qtd
     */
    public static void setQtd(int qtd) {
        Lanceiro.qtd = qtd;
    }
    
    /**
     * Incrementa a vida dos lanceiros
     */
    public static void maisVida(){
        ++perVida;
    }

    /**
     * Diminui a vida dos lanceiros
     */
    public static void menosVida(){
        --perVida;
    }

    /**
     * Incrementa o ataque dos lanceiros
     */
    public static void maisAtaque(){
        ++perAtaque;
    }

    /**
     * Diminui o ataque dos lanceiros
     */
    public static void menosAtaque(){
        --perAtaque;
    }

    /**
     * Retorna a vida adicional dos lanceiros
     * @return
     */
    public static int getPerVida() {
        return perVida;
    }

    /**
     * Seta a vida adicional dos lanceiros
     * @param perVida
     */
    public static void setPerVida(int perVida) {
        Lanceiro.perVida = perVida;
    }

    /**
     * Retorna o ataque adicional dos lanceiros
     * @return
     */
    public static int getPerAtaque() {
        return perAtaque;
    }

    /**
     * Seta o ataque adicional dos lanceiros
     * @param perAtaque
     */
    public static void setPerAtaque(int perAtaque) {
        Lanceiro.perAtaque = perAtaque;
    }

    /**
     * Estabelece o ataque real dos lanceiros, pela soma do dano base mais incrementos
     * @return
     */
    public int getDanoPorAtaqueCustom() {
        return danoPorAtaque+perAtaque;
    }

    /**
     * Estabelece a vida real dos lanceiros, pela soma do dano base mais incrementos
     * @return
     */
    public int getPontosDeVidaCustom() {
        return pontosDeVida+perVida;
    }
    @Override
    public String toString() {
        return "Atributos: " + "Pontos De Vida: " + pontosDeVida + "; Armadura: " + armadura + "; Penetração De Armadura: " + penetracaoDeArmadura + "; Dano Por Ataque: " + danoPorAtaque + "; Consegue Atacar de Longe: " + longRange;
    }

    /**
     * Retorna o dano base dos lanceiros
     * @return
     */
    public static int getDanoNormal(){
        return 5;
    }

    /**
     * Retorna a vida base dos lanceiros
     * @return
     */
    public static int getVidaNormal(){
        return 6;
    }
    
    /**
     * Retorna o estado do movimento especial dos lanceiros
     * @return
     */
    public static boolean isMovimentoEspecial() {
        return Lanceiro.movimentoEspecial;
    }

    /**
     * Seta o movimento especial dos lanceiros
     * @param a
     */
    public static void setMovimentoEspecial(boolean a) {
        Lanceiro.movimentoEspecial = a;
    }
    
    /**
     * Verifica se o lanceiro pode receber dano
     * @param a
     * @return
     */
    @Override
    public boolean receberDano (Unidade a){
       if (this.pontosDeVida<=0){
            return true;
        }
        else{
        this.pontosDeVida-=calculoDano(a);
        if (this.pontosDeVida<=0)
            return true;
        else
            return false;
        } 
    }

    /**
     * Retorna o dano recebido pelo guerreiro
     * @param a
     * @return
     */
    
}
