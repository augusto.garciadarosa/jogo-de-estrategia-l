/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhoestrategia.model;

import java.util.ArrayList;

/**
 * Classe que controla o exercito
 * @author Raynan
 */
public class Exercito {
    private ArrayList<Unidade> integrantes = new ArrayList<Unidade>();

    /**
     * Construtor do exercito
     */
    public Exercito() {
    }
    
    /**
     * Arraylist com os integrantes do exercito
     * @return
     */
    public ArrayList<Unidade> getIntegrantes() {
        return integrantes;
    }

    /**
     * Função que seta os integrantes
     * @param integrantes
     */
    public void setIntegrantes(ArrayList<Unidade> integrantes) {
        this.integrantes = integrantes;
    }

    /**
     * Funcão que da o golpe na unidade, de acordo com o atacado, e o dano recebido
     * @param a
     * @param c
     * @return
     */
    public boolean receberGolpe(Unidade a, int c){
       
        if (integrantes.get(c).receberDano(a)==true||integrantes.get(c).getPontosDeVida()<=0){
            integrantes.remove(c);
            return true;
        }
        else {
            Unidade b = integrantes.get(c);
            integrantes.remove(c);
            integrantes.add(b);
            return false;
        }
    }

    /**
     * Atualiza o número de unidades
     */
    public void atualizarUnidadesCustom(){
            for (Unidade i : integrantes){
                if (i instanceof Guerreiro){
                    i.setPontosDeVida(i.getPontosDeVida()+Guerreiro.getPerVida());
                    i.setDanoPorAtaque(i.getDanoPorAtaque()+Guerreiro.getPerAtaque());
                }
                if (i instanceof Bastiao){
                    i.setPontosDeVida(i.getPontosDeVida()+Bastiao.getPerVida());
                    i.setDanoPorAtaque(i.getDanoPorAtaque()+Bastiao.getPerAtaque());
                }
                if (i instanceof Lanceiro){
                    i.setPontosDeVida(i.getPontosDeVida()+Lanceiro.getPerVida());
                    i.setDanoPorAtaque(i.getDanoPorAtaque()+Lanceiro.getPerAtaque());
                }
                if (i instanceof Arqueiro){
                    i.setPontosDeVida(i.getPontosDeVida()+Arqueiro.getPerVida());
                    i.setDanoPorAtaque(i.getDanoPorAtaque()+Arqueiro.getPerAtaque());
                }
            }
    }

    /**
     * Pega a vida total do exercito
     * @return
     */
    public int getVidaTotal(){
        int vida=0;
        for (Unidade i : integrantes){
            if (i instanceof Guerreiro)
                vida+=((Guerreiro) i).getPontosDeVida();
            if (i instanceof Lanceiro)
                vida+=((Lanceiro) i).getPontosDeVida();
            if (i instanceof Bastiao)
                vida+=((Bastiao) i).getPontosDeVida();
            if (i instanceof Arqueiro)
                vida+=((Arqueiro) i).getPontosDeVida();
            if (i instanceof Zombie)
                vida+=((Zombie) i).getPontosDeVida();
        }
        return vida;
    }

    /**
     * Pega o tipo dos integrantes da batalha
     * @return
     */
    public int[] getTipoIntegrantes(){
        int[] tipos = new int[4];
        tipos[0]=0;
        tipos[1]=0;
        tipos[2]=0;
        tipos[3]=0;
        for (Unidade i : integrantes){
                if (i instanceof Guerreiro)
                    tipos[0]++;
                if (i instanceof Bastiao){
                    tipos[1]++;
                }
                if (i instanceof Lanceiro){
                    tipos[2]++;
                }
                if (i instanceof Arqueiro){
                    tipos[3]++;
                }
            }
        return tipos;
    }
    
}
